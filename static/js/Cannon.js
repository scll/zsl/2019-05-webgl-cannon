class Cannon3D {

    constructor(color) {
        console.log("Cannon3D.js działa")
        this.container = new THREE.Object3D()
        this.container2 = new THREE.Object3D()
        const material1 = new THREE.MeshBasicMaterial({
            transparent: false,
            wireframe: true,
            color: color,
            side: THREE.DoubleSide,
        });
        const wheelGeometry = new THREE.CylinderGeometry(30, 30, 10, 120);
        this.wheel1 = new THREE.Mesh(wheelGeometry, material1)
        this.wheel1.position.x = 15
        this.wheel1.rotation.z = Math.PI / 2
        // this.wheel1.rotation.z = Math.PI / 2
        this.wheel2 = new THREE.Mesh(wheelGeometry, material1)
        this.wheel2.position.x = -15
        this.wheel2.rotation.z = Math.PI / 2
        // this.wheel2.rotation.z = Math.PI / 2

        const cannonGeometry = new THREE.CylinderGeometry(10, 10, localData.cannonLength, 152);
        this.cannon1 = new THREE.Mesh(cannonGeometry, material1)
        this.cannon1.name = "cannonName"
        // this.cannon1.rotation.x = Math.PI / 4
        // this.cannon.rotation.z = Math.PI / 2
        this.cannon1.geometry.translate(0, 30, 0)

        // this.container.rotation.y = Math.PI / 3
        const axes = new THREE.AxesHelper(200)
        this.container.add(axes)
        this.container2.add(this.wheel1)
        this.container2.add(this.wheel2)
        this.container2.add(this.cannon1)
        this.container2.rotation.y = Math.PI / 2
        this.container.add(this.container2)
        return this.container
    }
}