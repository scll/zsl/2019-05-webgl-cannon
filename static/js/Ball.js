class CannonBall3D {

    constructor() {
        console.log("Cannon3D.js działa")
        // this.container = new THREE.Object3D();
        const material1 = new THREE.MeshNormalMaterial({
            transparent: false,
            // wireframe: true,
            // color: 0xff00ff,
            side: THREE.DoubleSide,
        });
        const sphereGeometry = new THREE.SphereGeometry(10);
        this.cannonBall1 = new THREE.Mesh(sphereGeometry, material1)
        this.cannonBall1.geometry.translate(0, 30, 0)
        this.cannonBall1.position.y = 60
        // this.cannonBall1.rotation.x = Math.PI / 4
        // this.cannonBall1.name = "sphereName"
        return this.cannonBall1
    }
}