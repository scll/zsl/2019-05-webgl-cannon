$(document).ready(function () {
    var client = io();
    client.on("onconnect", function (data) {
        console.log(data.player)

        const scene = new THREE.Scene();
        const widt = $(window).width();
        const heig = $(window).height();
        const camera = new THREE.PerspectiveCamera(
            45,    // kąt patrzenia kamery (FOV - field of view)
            widt / heig,    // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
            0.1,    // minimalna renderowana odległość
            10000    // maxymalna renderowana odległość od kamery 
        );
        let renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor(0xcccccc);
        renderer.setSize(widt, heig);
        $("#root").append(renderer.domElement);
        let axes = new THREE.AxesHelper(1000)
        scene.add(axes)
        camera.position.set(300, 220, -300)
        camera.lookAt(scene.position)
        let geometryPlane = new THREE.PlaneGeometry(2000, 2000, 50, 50);
        let materialPlane = new THREE.MeshNormalMaterial({
            wireframe: true,
            side: THREE.DoubleSide,
        })
        let plane = new THREE.Mesh(geometryPlane, materialPlane);
        plane.position.y = 0
        plane.rotation.x = Math.PI / 2
        scene.add(plane);
        const orbitControl = new THREE.OrbitControls(camera, renderer.domElement);
        orbitControl.addEventListener('change', function () {
            renderer.render(scene, camera)
        });

        createCannon(data.player)
        const { currentPlayer, otherPlayer } = localData

        $("#dzialo").on("input", function () { //nachylenie działa
            let value = $("#dzialo").val()
            localData.beta[currentPlayer] = -value * (Math.PI / 180) + (Math.PI / 2)
            cannonPos(value, true)
            // console.log(localData.beta)
            // cannonBallPos()
            client.emit("dzialo1", {
                nachylenie: value
            })
        })
        $("#cannon").on("input", function () { //obrót armaty
            let value = $("#cannon").val()
            localData.alpha[currentPlayer] = value * (Math.PI / 180) //+ (Math.PI)
            cannonRot(value, true)
            // console.log(localData.alpha)
            // cannonBallPos()
            client.emit("cannon1", {
                rotAngle: value
            })
        })
        $("#shoot").on("click", function () {
            cannonBallPos(true)
            localData.time[currentPlayer] = 0
            // let dzialo1 = cannon.getObjectByName("cannonName")
            // console.log(dzialo1.getWorldDirection())
            // localData.vector = dzialo1.getWorldDirection()
            // localData.startPos = cannonBall.position
            localData.flying[currentPlayer] = true
            client.emit("shoot1", {})
        })

        function cannonBallPos() {
            if (localData.currentPlayer == 0) {
                localData.balls[currentPlayer].position.x = localData.cannonLength * Math.cos(localData.beta[currentPlayer]) * Math.cos(localData.alpha[currentPlayer])
                localData.balls[currentPlayer].position.y = localData.cannonLength * Math.sin(localData.beta[currentPlayer])// + 30
                localData.balls[currentPlayer].position.z = localData.cannonLength * Math.cos(localData.beta[currentPlayer]) * Math.sin(localData.alpha[currentPlayer]) + 50
                // localData.startPos[currentPlayer].x = localData.balls[currentPlayer].position.x
                // localData.startPos[currentPlayer].y = localData.balls[currentPlayer].position.y
                // localData.startPos[currentPlayer].z = localData.balls[currentPlayer].position.z

                localData.balls[otherPlayer].position.x = localData.cannonLength * Math.cos(localData.beta[otherPlayer]) * Math.cos(localData.alpha[otherPlayer])
                localData.balls[otherPlayer].position.y = localData.cannonLength * Math.sin(localData.beta[otherPlayer])// + 30
                localData.balls[otherPlayer].position.z = localData.cannonLength * Math.cos(localData.beta[otherPlayer]) * Math.sin(localData.alpha[otherPlayer]) - 50
                // localData.startPos[otherPlayer].x = localData.balls[otherPlayer].position.x
                // localData.startPos[otherPlayer].y = localData.balls[otherPlayer].position.y
                // localData.startPos[otherPlayer].z = localData.balls[otherPlayer].position.z
            }
            else if (localData.currentPlayer == 1) {
                localData.balls[currentPlayer].position.x = localData.cannonLength * Math.cos(localData.beta[currentPlayer]) * Math.cos(localData.alpha[currentPlayer])
                localData.balls[currentPlayer].position.y = localData.cannonLength * Math.sin(localData.beta[currentPlayer])// + 30
                localData.balls[currentPlayer].position.z = localData.cannonLength * Math.cos(localData.beta[currentPlayer]) * Math.sin(localData.alpha[currentPlayer]) - 50

                localData.balls[otherPlayer].position.x = localData.cannonLength * Math.cos(localData.beta[otherPlayer]) * Math.cos(localData.alpha[otherPlayer])
                localData.balls[otherPlayer].position.y = localData.cannonLength * Math.sin(localData.beta[otherPlayer])// + 30
                localData.balls[otherPlayer].position.z = localData.cannonLength * Math.cos(localData.beta[otherPlayer]) * Math.sin(localData.alpha[otherPlayer]) + 50
            }
            //cannonBall.geometry.translate(0, 40, 0)
            // console.log("ball")
        }
        function cannonPos(value, currPlay) {
            if (currPlay) { var dzialo1 = localData.players[currentPlayer].getObjectByName("cannonName"); cannonBallPos(true) }
            else if (!currPlay) { var dzialo1 = localData.players[otherPlayer].getObjectByName("cannonName"); cannonBallPos(false) }
            dzialo1.rotation.x = value * (Math.PI / 180)

        }
        function cannonRot(value, currPlay) {
            if (currPlay) { localData.players[currentPlayer].rotation.y = -value * (Math.PI / 180); cannonBallPos(true) }
            else if (!currPlay) { localData.players[otherPlayer].rotation.y = -value * (Math.PI / 180); cannonBallPos(false) }
            // cannonBallPos()
        }

        // let time = 0;
        function render() {
            camera.lookAt(scene.position)
            requestAnimationFrame(render);
            renderer.render(scene, camera);
            if (localData.flying[currentPlayer]) {
                if (localData.currentPlayer == 0) {
                    localData.time[currentPlayer] += 0.1
                    console.log("flying Current")
                    let time = localData.time[currentPlayer]
                    localData.balls[currentPlayer].position.x = 100 * time * Math.cos(localData.beta[currentPlayer]) * Math.cos(localData.alpha[currentPlayer]) //+ localData.startPos.x//* localData.vector.x 
                    localData.balls[currentPlayer].position.y = 100 * time * Math.sin(localData.beta[currentPlayer]) - ((9.81 * time * time) / 2) //+ 30 //+ localData.startPos.y 
                    localData.balls[currentPlayer].position.z = 100 * time * Math.cos(localData.beta[currentPlayer]) * Math.sin(localData.alpha[currentPlayer]) + 50//+ localData.startPos.z//localData.vector.z //+ cannon.position.z

                    if (localData.balls[currentPlayer].position.y <= -20) {
                        localData.flying[currentPlayer] = false
                    }
                }
                else if (localData.currentPlayer == 1) {
                    localData.time[currentPlayer] += 0.1
                    console.log("flying Current")
                    let time = localData.time[currentPlayer]
                    localData.balls[currentPlayer].position.x = 100 * time * Math.cos(localData.beta[currentPlayer]) * Math.cos(localData.alpha[currentPlayer]) //+ localData.startPos.x//* localData.vector.x 
                    localData.balls[currentPlayer].position.y = 100 * time * Math.sin(localData.beta[currentPlayer]) - ((9.81 * time * time) / 2) //+ 30 //+ localData.startPos.y 
                    localData.balls[currentPlayer].position.z = 100 * time * Math.cos(localData.beta[currentPlayer]) * Math.sin(localData.alpha[currentPlayer]) - 50//+ localData.startPos.z//localData.vector.z //+ cannon.position.z

                    if (localData.balls[currentPlayer].position.y <= -20) {
                        localData.flying[currentPlayer] = false
                    }
                }
            }
            if (localData.flying[otherPlayer]) {
                if (localData.currentPlayer == 0) {
                    localData.time[otherPlayer] += 0.1
                    console.log("flying Other")
                    let time = localData.time[otherPlayer]
                    localData.balls[otherPlayer].position.x = 100 * time * Math.cos(localData.beta[otherPlayer]) * Math.cos(localData.alpha[otherPlayer]) //+ localData.startPos.x//* localData.vector.x 
                    localData.balls[otherPlayer].position.y = 100 * time * Math.sin(localData.beta[otherPlayer]) - ((9.81 * time * time) / 2) //+ 30 //+ localData.startPos.y 
                    localData.balls[otherPlayer].position.z = 100 * time * Math.cos(localData.beta[otherPlayer]) * Math.sin(localData.alpha[otherPlayer]) - 50//+ localData.startPos.z//localData.vector.z //+ cannon.position.z

                    if (localData.balls[otherPlayer].position.y <= -20) {
                        localData.flying[otherPlayer] = false
                    }
                }
                else if (localData.currentPlayer == 1) {
                    localData.time[otherPlayer] += 0.1
                    console.log("flying Other")
                    let time = localData.time[otherPlayer]
                    localData.balls[otherPlayer].position.x = 100 * time * Math.cos(localData.beta[otherPlayer]) * Math.cos(localData.alpha[otherPlayer]) //+ localData.startPos.x//* localData.vector.x 
                    localData.balls[otherPlayer].position.y = 100 * time * Math.sin(localData.beta[otherPlayer]) - ((9.81 * time * time) / 2) //+ 30 //+ localData.startPos.y 
                    localData.balls[otherPlayer].position.z = 100 * time * Math.cos(localData.beta[otherPlayer]) * Math.sin(localData.alpha[otherPlayer]) + 50//+ localData.startPos.z//localData.vector.z //+ cannon.position.z

                    if (localData.balls[otherPlayer].position.y <= -20) {
                        localData.flying[otherPlayer] = false
                    }
                }
            }
        }
        render();

        client.on("dzialo1serv", function (data) {
            if (data.player == localData.currentPlayer) {
                localData.beta[currentPlayer] = -data.nachylenie * (Math.PI / 180) + (Math.PI / 2)
                cannonPos(data.nachylenie, true)
            }
            else {
                localData.beta[otherPlayer] = -data.nachylenie * (Math.PI / 180) + (Math.PI / 2)
                cannonPos(data.nachylenie, false)
            }
        })
        client.on("cannon1serv", function (data) {
            if (data.player == localData.currentPlayer) {
                localData.alpha[currentPlayer] = data.rotAngle * (Math.PI / 180)
                cannonRot(data.rotAngle, true)
            }
            else {
                localData.alpha[otherPlayer] = data.rotAngle * (Math.PI / 180)
                cannonRot(data.rotAngle, false)
            }

            // cannonRot(data.rotAngle, false)
            // console.log(data.rotAngle)
        })
        client.on("shoot1serv", function (data) {
            // cannonBallPos(false)
            localData.time[otherPlayer] = 0
            localData.flying[otherPlayer] = true
        })
        client.on("otherPlayerJoin", function (data) {
            if (localData.cokolwiek == 0) {
                createCannon("2");
                localData.cokolwiek++
            }
            console.log("otherPlayerJoin")
        })


        function createCannon(xx) {
            if (xx == "0") {
                localData.currentPlayer = 0
                localData.otherPlayer = 1
                let cannon = new Cannon3D(0xff00ff)
                cannon.position.z = 50
                cannon.position.y = 30
                localData.players[0] = cannon
                scene.add(localData.players[0])
                let cannonBall = new CannonBall3D()
                cannonBall.position.z = 50
                localData.balls[0] = cannonBall
                scene.add(localData.balls[0]);

            }
            else if (xx == "1") {
                localData.currentPlayer = 1
                localData.otherPlayer = 0

                let cannon = new Cannon3D(0xff00ff)
                cannon.position.z = 50
                cannon.position.y = 30
                localData.players[0] = cannon
                scene.add(localData.players[0])
                let cannonBall = new CannonBall3D()
                cannonBall.position.z = 50
                localData.balls[0] = cannonBall
                scene.add(localData.balls[0]);

                let cannon1 = new Cannon3D(0xf0ff0f)
                cannon1.position.z = -50
                cannon1.position.y = 30
                localData.players[1] = cannon1
                scene.add(localData.players[1]);
                let cannonBall1 = new CannonBall3D()
                cannonBall1.position.z = -50
                localData.balls[1] = cannonBall1
                scene.add(localData.balls[1]);
            }
            else if (xx == "2") {
                let cannon1 = new Cannon3D(0xf0ff0f)
                cannon1.position.z = -50
                cannon1.position.y = 30
                localData.players[1] = cannon1
                scene.add(localData.players[1]);
                let cannonBall1 = new CannonBall3D()
                cannonBall1.position.z = -50
                localData.balls[1] = cannonBall1
                scene.add(localData.balls[1]);
            }
        }
    })
})