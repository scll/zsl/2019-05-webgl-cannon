const localData = {
    currentPlayer: undefined,
    cannonLength: 60,
    time: [0, 0],
    alpha: [0, 0],
    beta: [1.5707963267948966, 1.5707963267948966],
    flying: [false, false],
    players: [undefined, undefined],
    balls: [undefined, undefined],
    nachylenie: [undefined, undefined],
    obrot: [undefined, undefined],
    cokolwiek: 0,
    startPos: [{ x: undefined, y: undefined, z: undefined }, { x: undefined, y: undefined, z: undefined }]
}