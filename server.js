var express = require("express")
var app = express()
var http = require("http").createServer(app);
var bodyParser = require("body-parser")
var socketio = require('socket.io')(http);
const PORT = 3000;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('static'))
app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname + "/static/index.html"))
})

var players = [undefined, undefined]
// var nachylenie = [undefined, undefined]
// var obrot = [undefined, undefined]

socketio.on('connection', function (client) {
    console.log("klient się podłączył " + client.id)
    if (!players[0]) {
        players[0] = client.id
        client.emit("onconnect", {
            player: "0"
        });
    }
    else if (!players[1]) {
        players[1] = client.id
        client.emit("onconnect", {
            player: "1"
        });
        client.broadcast.emit("otherPlayerJoin", { player: "1" });
    }
    console.log(players)
    client.on("dzialo1", function (data) { //nachylenie dziala
        // console.log(data.nachylenie + " - ")
        if (players[0] == client.id) {
            client.broadcast.emit("dzialo1serv", { nachylenie: data.nachylenie, player: "0" });
        }
        else if (players[1] == client.id) {
            client.broadcast.emit("dzialo1serv", { nachylenie: data.nachylenie, player: "1" });
        }
    })
    client.on("cannon1", function (data) { //obrot dziala
        // console.log(data.rotAngle + " - ")
        if (players[0] == client.id) {
            // console.log("obrot playera 0")
            client.broadcast.emit("cannon1serv", { rotAngle: data.rotAngle, player: "0" });
        }
        else if (players[1] == client.id) {
            // console.log("obrot playera 1")
            client.broadcast.emit("cannon1serv", { rotAngle: data.rotAngle, player: "1" });
        }
    })
    client.on("shoot1", function (data) { //strzal
        if (players[0] == client.id) {
            client.broadcast.emit("shoot1serv", { player: "0" });
        }
        else if (players[1] == client.id) {
            client.broadcast.emit("shoot1serv", { player: "1" });
        }
    })
    client.on("disconnect", function () {
        console.log("dany klient się rozłączył: " + client.id)
        if (players[0] == client.id) {
            players[0] = undefined
        }
        else if (players[1] == client.id) {
            players[1] = undefined
        }
    });
});

// var level;
// app.post("/saveData", function (req, res) {
//     level = req.body;
//     res.send(JSON.stringify(req.body))
// })

http.listen(PORT, function () {
    console.log("start serwera na porcie " + PORT)
})